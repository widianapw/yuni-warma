import Head from 'next/head'
import Image from 'next/image'
import {Inter} from '@next/font/google'
import styles from '../styles/Home.module.css'

const inter = Inter({subsets: ['latin']})

export default function Home() {
    const _images = [
        "/1.webp",
        "/2.webp",
        "/3.webp",
        "/4.webp",
        "/5.webp",
        "/6.webp",
        "/7.webp",
        "/8.webp",
        "/9.webp",
        "/10.webp",
        "/11.webp",
        "/12.webp"
    ]
    return (
        <>
            <Head>
                <title>Yuni Sartika Dewi</title>
                <meta name="description" content="#MAJUBE2SAMA #BETHEBEST #BETHEWINNER"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <meta property="og:image" content="/logo.webp"/>
                <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png"/>
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png"/>
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png"/>
                <link rel="manifest" href="/favicon/site.webmanifest"/>
            </Head>
            <main>
                <div className={"flex flex-col items-center bg-primary"}>
                    {
                        _images.map((image, index) => {
                            return <img src={image} alt="" data-aos="fade-up"/>
                        })
                    }
                </div>
            </main>
        </>
    )
}
